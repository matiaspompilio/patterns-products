package ar.edu.unlp.pas.patternsproducts.exceptions;

public class InvalidDataException extends RuntimeException{
    public InvalidDataException(final String message) {
        super(message);
    }
}

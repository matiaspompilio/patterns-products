package ar.edu.unlp.pas.patternsproducts.domain.model;

import ar.edu.unlp.pas.patternsproducts.domain.dto.PublicationDTO;
import ar.edu.unlp.pas.patternsproducts.domain.dto.mapper.PublicationMapper;
import ar.edu.unlp.pas.patternsproducts.domain.enums.PublicationStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@AllArgsConstructor
public class Publication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("name")
    @NotBlank(message = "Name is mandatory")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("price")
    @PositiveOrZero
    @NotNull(message = "Price is mandatory")
    private double price;

    @JsonProperty("category")
    private String category;

    @JsonProperty("stock")
    @PositiveOrZero
    private Long stock;

    @JsonProperty("status")
    private PublicationStatus status;

    @JsonProperty("addressId")
    @NotNull(message = "Address is mandatory")
    private Long addressId;

    @JsonProperty("sellerId")
    @NotNull(message = "Seller is mandatory")
    private Long sellerId;

    public PublicationDTO toDTO() {
        return PublicationMapper.INSTANCE.toDTO(this);
    }

}

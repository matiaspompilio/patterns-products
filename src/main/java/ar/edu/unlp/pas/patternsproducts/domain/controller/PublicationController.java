package ar.edu.unlp.pas.patternsproducts.domain.controller;

import ar.edu.unlp.pas.patternsproducts.domain.dto.PublicationDTO;
import ar.edu.unlp.pas.patternsproducts.domain.dto.SingleLongDTO;
import ar.edu.unlp.pas.patternsproducts.domain.model.Publication;
import ar.edu.unlp.pas.patternsproducts.domain.service.PublicationService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/publication")
public class PublicationController {

    private final PublicationService publicationService;

    @PostMapping
    public ResponseEntity<PublicationDTO> registerNewProduct(@RequestBody @Valid final Publication publication) {
        return ResponseEntity.ok(publicationService.createNew(publication));
    }

    @GetMapping()
    public ResponseEntity<List<PublicationDTO>> getPublicationsByName(@RequestParam(value = "search", required = false) String keyword) {
        return ResponseEntity.ok(publicationService.getPublicationsByNameOrDescription(keyword));
    }

    @GetMapping("{publicationId}")
    public ResponseEntity<PublicationDTO> getById(@PathVariable("publicationId") long publicationId) {
        return ResponseEntity.ok(publicationService.getPublicationAsDTOById(publicationId));
    }

    @PutMapping("/{publicationId}/stock")
    public ResponseEntity<PublicationDTO> setStock(@PathVariable("publicationId") long publicationId, @RequestBody long stock) {
        return ResponseEntity.ok(publicationService.setStock(publicationId, stock));
    }

    @PutMapping("/{publicationId}/start")
    public ResponseEntity<PublicationDTO> start(@PathVariable("publicationId") long publicationId) {
        return ResponseEntity.ok(publicationService.start(publicationId));
    }

    @PutMapping("/{publicationId}/pause")
    public ResponseEntity<PublicationDTO> pause(@PathVariable("publicationId") long publicationId) {
        return ResponseEntity.ok(publicationService.pause(publicationId));
    }

    @PutMapping("/{publicationId}/finish")
    public ResponseEntity<PublicationDTO> finish(@PathVariable("publicationId") long publicationId) {
        return ResponseEntity.ok(publicationService.finish(publicationId));
    }
}

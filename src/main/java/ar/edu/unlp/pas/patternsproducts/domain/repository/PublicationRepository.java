package ar.edu.unlp.pas.patternsproducts.domain.repository;

import ar.edu.unlp.pas.patternsproducts.domain.model.Publication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PublicationRepository extends JpaRepository<Publication, Long> {

    Optional<Publication> findPublicationById(long id);

    List<Publication> findByNameContainingOrDescriptionContaining(String name, String description);

}

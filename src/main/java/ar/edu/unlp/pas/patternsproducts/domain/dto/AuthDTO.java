package ar.edu.unlp.pas.patternsproducts.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthDTO {
    String requestUri;
}

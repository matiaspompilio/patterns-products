package ar.edu.unlp.pas.patternsproducts.domain.dto.mapper;

import ar.edu.unlp.pas.patternsproducts.domain.dto.PublicationDTO;
import ar.edu.unlp.pas.patternsproducts.domain.model.Publication;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PublicationMapper {

    PublicationMapper INSTANCE = Mappers.getMapper(PublicationMapper.class);

    @Mapping(source = "name", target = "name")
    @Mapping(target = "address", ignore = true)
    @Mapping(target = "seller", ignore = true)
    PublicationDTO toDTO(Publication publication);
}

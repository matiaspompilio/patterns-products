package ar.edu.unlp.pas.patternsproducts.domain.utils;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectionUtils {
    public static <A, B> List<B> mapList(Collection<A> list, Function<? super A, B> mapFunction) {
        return list.stream().map(mapFunction).collect(Collectors.toList());
    }
}

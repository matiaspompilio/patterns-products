package ar.edu.unlp.pas.patternsproducts.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PersonDTO {
  Long id;
  String fullName;
  int age;
  AddressDTO invoiceAddress;
  List<AddressDTO> shippingAddress;
}

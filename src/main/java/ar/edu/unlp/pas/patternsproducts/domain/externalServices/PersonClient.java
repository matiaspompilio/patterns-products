package ar.edu.unlp.pas.patternsproducts.domain.externalServices;

import ar.edu.unlp.pas.patternsproducts.domain.dto.PersonDTO;
import ar.edu.unlp.pas.patternsproducts.exceptions.ResourceNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class PersonClient {

    @Value("${idp.uri}")
    private String uri;

    private final ObjectMapper mapper = new ObjectMapper();
    private static final String PERSON = "Person";
    private final HttpClient httpClient = HttpClient.newHttpClient();

    public PersonDTO getPerson(Long personId) {
        String jwt = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getHeader("authorization");
        HttpRequest request = HttpRequest.newBuilder().setHeader("authorization", jwt)
            .GET().uri(URI.create(uri + "/api/v1/person/" + personId)).build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            return mapper.readValue(response.body(), PersonDTO.class);
        } catch (IOException | InterruptedException e) {
            throw new ResourceNotFoundException(PERSON, "id", personId);
        }
    }

}

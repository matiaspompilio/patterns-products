package ar.edu.unlp.pas.patternsproducts.domain.service;

import ar.edu.unlp.pas.patternsproducts.domain.dto.PersonDTO;
import ar.edu.unlp.pas.patternsproducts.domain.dto.PublicationDTO;
import ar.edu.unlp.pas.patternsproducts.domain.enums.PublicationStatus;
import ar.edu.unlp.pas.patternsproducts.domain.model.Publication;
import ar.edu.unlp.pas.patternsproducts.domain.repository.PublicationRepository;
import ar.edu.unlp.pas.patternsproducts.domain.utils.CollectionUtils;
import ar.edu.unlp.pas.patternsproducts.exceptions.InvalidDataException;
import ar.edu.unlp.pas.patternsproducts.exceptions.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@AllArgsConstructor
@Service
@Transactional
@Slf4j
public class PublicationService {

    private static final String PUBLICATION = "Publication";

    private final PublicationRepository publicationRepository;
    private final PersonService personService;

    public Publication save(Publication publication) {
        return publicationRepository.save(publication);
    }

    private PublicationDTO getPublicationDTO(Publication publication) {
        PersonDTO personDTO = personService.getPerson(publication.getSellerId());
        if (!this.personService.validateAddressForPerson(personDTO, publication.getAddressId())) {
            throw new InvalidDataException("Address doesnt belong to the seller.");
        }

        PublicationDTO publicationDTO = publication.toDTO();
        publicationDTO.setAddress(personDTO.getShippingAddress().stream().filter(a -> a.getId() == publication.getAddressId()).findFirst().orElseThrow());
        publicationDTO.setSeller(personDTO);
        return publicationDTO;
    }

    public PublicationDTO createNew(Publication publication) {
        publication.setStatus(PublicationStatus.PAUSED);
        PublicationDTO publicationDTO = this.getPublicationDTO(publication);
        this.save(publication);
        publicationDTO.setId(publication.getId());
        return publicationDTO;
    }

    public Publication getById(final long id) {
        return publicationRepository.findPublicationById(id)
                .orElseThrow(() -> new ResourceNotFoundException(PUBLICATION, "id", id));
    }

    public PublicationDTO getPublicationAsDTOById(Long id) {
        return this.getPublicationDTO(this.getById(id));
    }

    private Publication setState(Long publicationId, PublicationStatus status) {
        Publication publication = this.getById(publicationId);
        publication.setStatus(status);
        return this.publicationRepository.save(publication);
    }

    public PublicationDTO pause(long publicationId) {
        return this.getPublicationDTO(this.setState(publicationId, PublicationStatus.PAUSED));
    }

    public PublicationDTO start(long publicationId) {
        return this.getPublicationDTO(this.setState(publicationId, PublicationStatus.ACTIVE));
    }

    public PublicationDTO finish(long publicationId) {
        return this.getPublicationDTO(this.setState(publicationId, PublicationStatus.FINISHED));
    }

    public PublicationDTO setStock(long publicationId, Long number) {
        Publication publication = this.getById(publicationId);
        publication.setStock(number);
        this.publicationRepository.save(publication);
        return this.getPublicationDTO(publication);
    }

    public List<PublicationDTO> getPublicationsByNameOrDescription(String keyword) {
        List<Publication> publications = (keyword == null)
                ? publicationRepository.findAll()
                : publicationRepository.findByNameContainingOrDescriptionContaining(keyword, keyword);
        return CollectionUtils.mapList(publications, this::getPublicationDTO);
    }
}

package ar.edu.unlp.pas.patternsproducts.domain.dto;

import ar.edu.unlp.pas.patternsproducts.domain.enums.PublicationStatus;
import lombok.*;

@Getter
@Setter
public class PublicationDTO {
    Long id;
    String name;
    String description;
    double price;
    String category;
    Long stock;
    PublicationStatus status;
    AddressDTO address;
    PersonDTO seller;
}
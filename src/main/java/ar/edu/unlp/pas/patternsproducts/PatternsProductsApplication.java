package ar.edu.unlp.pas.patternsproducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatternsProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatternsProductsApplication.class, args);
	}

}

package ar.edu.unlp.pas.patternsproducts.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDTO {
    private long id;
    private String street;
    private Integer number;
    private Integer floor;
    private String apartment;
    private String city;
    private String province;
    private String country;
}

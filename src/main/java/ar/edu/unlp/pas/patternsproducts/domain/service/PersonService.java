package ar.edu.unlp.pas.patternsproducts.domain.service;

import ar.edu.unlp.pas.patternsproducts.domain.dto.AddressDTO;
import ar.edu.unlp.pas.patternsproducts.domain.dto.PersonDTO;
import ar.edu.unlp.pas.patternsproducts.domain.externalServices.PersonClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@AllArgsConstructor
@Service
@Transactional
@Slf4j
public class PersonService {

    private final PersonClient personClient;

    public PersonDTO getPerson(Long sellerId) {
        return personClient.getPerson(sellerId);
    }

    public boolean validateAddressForPerson(PersonDTO person, Long addressId) {
        return person.getShippingAddress().stream().map(AddressDTO::getId).anyMatch(a -> a.equals(addressId));
    }
}

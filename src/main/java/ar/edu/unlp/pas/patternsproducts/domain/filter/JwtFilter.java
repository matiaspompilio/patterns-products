package ar.edu.unlp.pas.patternsproducts.domain.filter;

import ar.edu.unlp.pas.patternsproducts.domain.dto.AuthDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.*;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Component
@Order(1)
public class JwtFilter extends GenericFilterBean {
    @Value("${idp.uri}")
    private String idpUri;

    @Bean
    public RestTemplate restTemplate(List<HttpMessageConverter<?>> messageConverters) {
        return new RestTemplate(messageConverters);
    }

    @Bean
    public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        return new ByteArrayHttpMessageConverter();
    }
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;
        final String authHeader = request.getHeader("authorization");

        if(authHeader == null || !authHeader.startsWith("Bearer ")){
            response.setHeader("error", "Authorization needed");
            response.setStatus(FORBIDDEN.value());
            return;
        }

        String requestUri = request.getRequestURI().toString();

        try {
            String uri = idpUri + "/api/v1/auth/authorize";

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.add("authorization", authHeader);

            AuthDTO body = new AuthDTO();
            body.setRequestUri(requestUri);
            HttpEntity<AuthDTO> entity = new HttpEntity<>(body, headers);
            ResponseEntity<Boolean> authorizedResponse =
                    restTemplate.exchange(uri, HttpMethod.POST, entity, Boolean.class);

            if(!authorizedResponse.getBody()) {
                response.setHeader("error", "Authorization needed");
                throw new ResponseStatusException(FORBIDDEN, "Not authorized for this endpoint");
            }

        } catch (Exception exception) {
            logger.error("Error logging in {}", exception);
            response.setHeader("error", exception.getMessage());
            response.setStatus(FORBIDDEN.value());
            final Map<String, String> error = Map.of(
                    "error_message", exception.getMessage()
            );
            response.setContentType(APPLICATION_JSON_VALUE);
            return;
        }


        filterChain.doFilter(request, response);
    }
}
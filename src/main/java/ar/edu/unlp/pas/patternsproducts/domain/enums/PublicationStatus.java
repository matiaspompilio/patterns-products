package ar.edu.unlp.pas.patternsproducts.domain.enums;

public enum PublicationStatus {
    ACTIVE,
    PAUSED,
    FINISHED
}